<?php
namespace Ciebit;

use DOMDocument;
use DOMNodeList;
use Gt\Dom\Element;
use Gt\Dom\HTMLCollection;
use Gt\Dom\HTMLDocument;

use function file_get_contents;
use function libxml_clear_errors;
use function libxml_use_internal_errors;

class DomTemplate
{
    public const EVENT_ADD_ELEMENT = 'add-element';
    public const EVENT_ADD_HTML = 'add-html';
    public const EVENT_CREATE_ELEMENT = 'create-element';
    public const EVENT_SET_HTML = 'set-html';

    private $css_code; #string
    private $Dom; #HTMLDocument
    private $events; #Array
    private $html; #string
    private $template; #string
    private $template_path; #string

    public function __construct()
    {
        $this->css_code = '';
        $this->events = [];
        $this->html = "";
        $this->template = "";
        $this->template_path = "";
    }

    public function addElement(string $selectorDestiny, Element $element): self
    {
        $Dom = $this->getDom();
        $container = $Dom->querySelector($selectorDestiny);
        if (!$container) {
            return $this;
        }

        $Fragment = $Dom->importNode($element, true);
        $container->appendChild($Fragment);

        $this->dispatchEvent(self::EVENT_ADD_ELEMENT, $Fragment, $this);

        return $this;
    }

    public function addElementsCollection(string $selectorDestiny, HTMLCollection $elements): self
    {
        while($elements->valid()) {
            $this->addElement($selectorDestiny, $elements->current());
            $elements->next();
        }

        return $this;
    }

    public function addEventListener(string $event, callable $function): self
    {
        if (! isset($this->events[$event])) {
            $this->events[$event] = [];
        }
        $this->events[$event][] = $function;
        return $this;
    }

    public function addHtml(string $selectorDestiny, string $str_html): self
    {
        $Dom = $this->getDom();
        $Container = $Dom->querySelector($selectorDestiny);
        if (! $Container) {
            return $this;
        }
        $html = "<!DOCTYPE html>
        <html><head>
        <meta charset=\"utf-8\"/>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
        </head><body>
        <div>{$str_html}</div>
        </body></html>";

        $HtmlDocument = new HTMLDocument;
        $HtmlDocument->strictErrorChecking = false;
        libxml_use_internal_errors(true);
        $HtmlDocument->loadHTML($html);
        libxml_clear_errors();
        $HtmlContainer = $HtmlDocument->querySelector('div');
        $Fragment = $Dom->importNode($HtmlContainer, true);
        $Container->appendChild($Fragment);

        $this->dispatchEvent(self::EVENT_ADD_HTML, $str_html, $Fragment, $this);

        return $this;
    }

    public function createElement(
        string $tag,
        array $attributes = [],
        string $text = '',
        string $destiny = 'body'
    ) {
        $Dom = $this->getDom();

        $TagElement = $Dom->createElement($tag);
        $TagElement->textContent = $text;

        foreach ($attributes as $attribute => $value) {
            $TagElement->setAttribute($attribute, $value);
        };

        $this->dispatchEvent(
            self::EVENT_CREATE_ELEMENT,
            $TagElement,
            $this->getElement($destiny),
            $this
        );

        $this->addElement($destiny, $TagElement);

        return $this;
    }

    public function dispatchEvent(string $event, ...$parameters): self
    {
        if (! isset($this->events[$event])) {
            return $this;
        }

        $events = $this->events[$event];
        foreach ($events as $callback) {
            $callback(...$parameters);
        }

        return $this;
    }

    public function getDom(): HTMLDocument
    {
        if ($this->Dom) {
            return $this->Dom;
        }

        $this->Dom = new HTMLDocument;
        $this->Dom->strictErrorChecking = false;

        if ($this->template) {
            libxml_use_internal_errors(true);
            $this->Dom->loadHTML($this->template, LIBXML_NOERROR);
            libxml_clear_errors();
        }

        return $this->Dom;
    }

    public function getElement(string $selector): ?Element
    {
        $Dom = $this->getDom();
        $node = $Dom->querySelector($selector);
        return $node;
    }

    public function getElementsAll(string $selector): HTMLCollection
    {
        $Dom = $this->getDom();
        return $Dom->querySelectorAll($selector);
    }

    public function getElementsChild(string $selector): HTMLCollection
    {
        $childNodes = new DOMNodeList;

        $parent = $this->getElement($selector);
        if ($parent) {
            $childNodes = $parent->childNodes;
        }

        return new HTMLCollection($childNodes);
    }

    public function getHtml(): string
    {
        if ($this->html) {
            return $this->html;
        }
        $this->html = $this->getDom()->saveHtml();
        return $this->html;
    }

    public function save(string $pathFile): bool
    {
        $state = $this->getDom()->saveHTMLFile($pathFile);

        return (bool) $state;
    }

    public function setHtmlByFile(string $template): self
    {
        $this->template_path = $template;
        $this->setHtmlByString(file_get_contents($template));
        return $this;
    }

    public function setHtmlByString(string $html): self
    {
        $this->template = $html;
        $this->dispatchEvent(self::EVENT_SET_HTML, $html, $this);
        return $this;
    }
}
