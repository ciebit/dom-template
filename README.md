# Dom Template

Módulo gerenciador de elementos DOM.

## Exemplo 01

```
#!php
require 'vendor/autoload.php';

use Ciebit\DomTemplate;

$DomTemplate = new DomTemplate;
$DomTemplate
->setHtmlToFile('template.html')
->createElement('h1', [], 'Apresentation', 'body')
->addHtml('<p>Hello World</p>')
;

echo $DomTemplate->getHtml();
```

## Bugs

Problemas encontrados podem ser reportados em https://bitbucket.org/ciebit/dom-template/issues
