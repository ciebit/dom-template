<?php
use Ciebit\DomTemplate;
use PHPUnit\Framework\TestCase;

class CreateTest extends TestCase
{
    private $eventDispatchResult1 = '';
    private $eventDispatchResult2 = '';

    public function testEventParameters()
    {
        $DomTemplate = new DomTemplate;
        $DomTemplate->addEventListener('teste', function($msg1, $msg2){
            $this->eventDispatchResult1 = $msg1;
            $this->eventDispatchResult2 = $msg2;
        });

        $DomTemplate->dispatchEvent('teste', 'Hello', 'World');
        $this->assertEquals('Hello', $this->eventDispatchResult1);
        $this->assertEquals('World', $this->eventDispatchResult2);
    }

    public function testEventListener()
    {
        $ElementDom = '';
        $DomTemplate = new DomTemplate;
        $DomTemplate->addEventListener(
            $DomTemplate::EVENT_CREATE_ELEMENT,
            function($Element) use (&$ElementDom)
            {
                $ElementDom = $Element;
            }
        );
        $DomTemplate->createElement('div', [], 'Test');
        $this->assertEquals('Test', $ElementDom->textContent);
    }

    public function testAddHtml()
    {
        $DomTemplate = new DomTemplate;
        $DomTemplate->addHtml('body', '<div>Test</div>');
        $Node = $DomTemplate->getElement('div');
        $this->assertEquals('Test', $Node->textContent);
    }

    public function testAddElement()
    {
        $DomTemplate = new DomTemplate;
        $Div = $DomTemplate->getDom()->createElement('div');
        $Div->textContent = 'Test';
        $DomTemplate->addElement('body', $Div);
        $Div2 = $DomTemplate->getElement('div');
        $this->assertEquals('Test', $Div2->textContent);
    }

    public function testCreateElement()
    {
        $DomTemplate = new DomTemplate;
        $DomTemplate->createElement('div', ['lang' => 'pt-br'], 'Test', 'body');
        $Div = $DomTemplate->getElement('div');
        $this->assertEquals('Test', $Div->textContent);
        $this->assertEquals('pt-br', $Div->getAttribute('lang'));
    }

    public function testDoctype()
    {
        $DomTemplate = new DomTemplate;
        $DomTemplate->setHtmlByFile(__DIR__.'/example.html');
        $html = $DomTemplate->getHtml();
        $this->assertContains('<!DOCTYPE html>', $html);
    }

    public function testSave()
    {
        $pathFile = __DIR__.'/html/test-save.html';
        $DomTemplate = new DomTemplate;
        $DomTemplate->setHtmlByString('<html><body><div lang="pt-br">Test Save</div></body></html>');
        $DomTemplate->save($pathFile);
        $this->assertFileExists($pathFile);
        unlink($pathFile);
    }

    public function testSetHtmlByString()
    {
        $DomTemplate = new DomTemplate;
        $DomTemplate->setHtmlByString('<html><body><div lang="pt-br">Test</div></body></html>');
        $Div = $DomTemplate->getElement('div');
        $this->assertEquals('Test', $Div->textContent);
        $this->assertEquals('pt-br', $Div->getAttribute('lang'));
    }

    public function testSetHtmlByFile()
    {
        $DomTemplate = new DomTemplate;
        $DomTemplate->setHtmlByFile(__DIR__.'/example.html');
        $Div = $DomTemplate->getElement('p');
        $this->assertEquals('Test', $Div->textContent);
        $this->assertEquals('pt-br', $Div->getAttribute('lang'));
    }

    public function testStructure()
    {
        $file = __DIR__.'/example.html';

        $DomTemplate = new DomTemplate;
        $DomTemplate->setHtmlByFile($file);
        $html = file_get_contents($file);
        $htmlDom = $DomTemplate->getHtml();
        $this->assertEquals($html, $htmlDom);
    }
}
