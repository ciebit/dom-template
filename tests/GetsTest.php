<?php
use Ciebit\DomTemplate;
use Gt\Dom\HTMLCollection;
use PHPUnit\Framework\TestCase;

class GetsTest extends TestCase
{
    public function testGetAll()
    {
        $domTemplate = new DomTemplate;
        $domTemplate->setHtmlByFile(__DIR__.'/html/gets.html');
        $paragraphs = $domTemplate->getElementsAll('p');
        $this->assertEquals(2, $paragraphs->length);
    }

    public function testGetAllNoNodes()
    {
        $domTemplate = new DomTemplate;
        $domTemplate->setHtmlByFile(__DIR__.'/html/gets.html');
        $images = $domTemplate->getElementsAll('img');
        $this->assertInstanceOf(HTMLCollection::class, $images);
        $this->assertEquals(0, $images->length);
    }

    public function testGetElementsChild()
    {
        $domTemplate = new DomTemplate;
        $domTemplate->setHtmlByFile(__DIR__.'/html/gets.html');
        $elements = $domTemplate->getElementsChild('body');
        $this->assertEquals(3, $elements->length);
    }

    public function testGetElementsChildNoNodes()
    {
        $domTemplate = new DomTemplate;
        $domTemplate->setHtmlByFile(__DIR__.'/html/gets.html');
        $images = $domTemplate->getElementsChild('img');
        $this->assertInstanceOf(HTMLCollection::class, $images);
        $this->assertEquals(0, $images->length);
    }
}
