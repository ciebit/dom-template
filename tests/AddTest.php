<?php
use Ciebit\DomTemplate;
use PHPUnit\Framework\TestCase;

class AddTest extends TestCase
{
    public function testAddCollection()
    {
        $pageContent = new DomTemplate;
        $pageContent->setHtmlByFile(__DIR__.'/html/gets.html');
        $paragraphs = $pageContent->getElementsAll('p');

        $pagePrimary = new DomTemplate;
        $pagePrimary->addElementsCollection('body', $paragraphs);

        $this->assertEquals(2, $pagePrimary->getElementsAll('p')->length);
    }

    public function testAddCollectionNoContainer()
    {
        $pageContent = new DomTemplate;
        $pageContent->setHtmlByFile(__DIR__.'/html/gets.html');
        $paragraphs = $pageContent->getElementsAll('p');

        $pagePrimary = new DomTemplate;
        $pagePrimary->addElementsCollection('div', $paragraphs);

        $this->assertEquals(0, $pagePrimary->getElementsAll('p')->length);
    }
}
